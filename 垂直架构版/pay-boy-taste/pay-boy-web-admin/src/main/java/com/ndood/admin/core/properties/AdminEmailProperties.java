package com.ndood.admin.core.properties;

import lombok.Data;

/**
 * 邮件相关配置
 */
@Data
public class AdminEmailProperties {

	/**
	 * 激活链接
	 */
	private String activationUrl = "/user/regist/email_finish";
	/**
	 * 重设密码邮件
	 */
	private String chagnePwdUrl = "/user/forget/to_email_change_pwd";
	/**
	 * 邮件aes加密的key
	 */
	private String encryptKey = "payboyplus";
	/**
	 * 发件人
	 */
	private String from = "payboyplus@126.com";
}
