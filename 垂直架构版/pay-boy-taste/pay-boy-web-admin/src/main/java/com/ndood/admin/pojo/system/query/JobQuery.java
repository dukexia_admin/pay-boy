package com.ndood.admin.pojo.system.query;
import java.util.Date;

import com.ndood.admin.pojo.system.JobPo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
/**
 * 日志查询类
 */
@Getter @Setter
@EqualsAndHashCode(callSuper=false)
public class JobQuery extends JobPo {
	private static final long serialVersionUID = -6552331510867296754L;
	private Integer offset;
	private Integer limit;
	private String keywords;
	private Date startTime;
	private Date endTime;
	public int getPageNo() {
		return offset / limit;
	}
}