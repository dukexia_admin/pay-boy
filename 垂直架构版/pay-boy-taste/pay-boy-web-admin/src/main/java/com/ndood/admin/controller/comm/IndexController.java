package com.ndood.admin.controller.comm;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.ndood.admin.core.web.tools.SessionUtils;
import com.ndood.admin.pojo.system.dto.UserDto;
import com.ndood.admin.service.user.AccountInfoService;

/**
 * 主控制器类
 * @author ndood
 */
@Controller
public class IndexController {
	
	@Autowired
	private SessionUtils sessionUtils;

	@Autowired
	private AccountInfoService accountInfoService;
	
	@GetMapping("/")
	public String home(){
		return "redirect:index";
	}

	@GetMapping("/index")
	public String index(Model model,HttpServletRequest request) throws Exception{
		// 获取当前用户的信息
		UserDetails userDetails = sessionUtils.getLoginUserInfo();
		if(userDetails==null) {
			return "redirect:/user/to_login";
		}
		String userId = userDetails.getUsername();
		UserDto accountInfo = accountInfoService.getAccountSimpleInfoById(userId);
		
		String nickName = accountInfo.getNickName();
		String encryptMobile = accountInfo.getEncryptMobile();
		String encryptEmail = accountInfo.getEncryptEmail();
		if(!StringUtils.isBlank(nickName)) {
			model.addAttribute("nickName",nickName);
		}else if(!StringUtils.isBlank(encryptMobile)) {
			model.addAttribute("nickName",encryptMobile);
		}else if(!StringUtils.isBlank(encryptEmail)) {
			model.addAttribute("nickName",encryptEmail);
		}
		model.addAttribute("headImageUrl",accountInfo.getHeadImgUrl());
		return "index_page";
	}
	
	@GetMapping("/center")
	public String center(){
		return "common/center";
	}

	/**
	 * 操作成功跳转
	 */
	@GetMapping("/jump_success")
	public String jumpSuccess(String url, Model model){
		if(StringUtils.isBlank(url)){
			url = "center";
		}
		model.addAttribute("url", url);
		return "common/jump_success";
	}

	/**
	 * 操作失败跳转
	 */
	@GetMapping("/jump_failed")
	public String jumpFailed(String url, Model model){
		if(StringUtils.isBlank(url)){
			url = "center";
		}
		model.addAttribute("url", url);
		return "common/jump_failed";
	}
	
	/**
     * 404页面
     */
    @GetMapping(value = "/error/404")
    public String error_404() {
        return "error/404";
    }

    /**
     * 500页面
     */
    @GetMapping(value = "/error/500")
    public String error_500() {
        return "error/500";
    }
    
    /**
     * 507页面
     */
    @GetMapping(value = "/error/507")
    public String error_507() {
        return "error/507";
    }
    
}
