package com.ndood.admin.controller.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ndood.admin.pojo.comm.vo.AdminResultVo;
import com.ndood.admin.pojo.system.DepartmentPo;
import com.ndood.admin.pojo.system.dto.DepartmentDto;
import com.ndood.admin.pojo.system.dto.TreeDto;
import com.ndood.admin.service.system.SystemDepartmentService;

/**
 * 部门控制器类
 * @author ndood
 */
@Controller
public class SystemDepartmentController {
	
	@Autowired
	private SystemDepartmentService systemDepartmentService;

	/**
	 * 显示部门页
	 */
	@GetMapping("/system/department")
	public String toDepartmentPage(){
		return "system/department/department_page";
	}
	
	/**
	 * 显示添加对话框
	 */
	@GetMapping("/system/department/add")
	public String toAddDepartment(Integer parentId,String parentName, Model model){
		if(!StringUtils.isEmpty(parentId)&&!StringUtils.isEmpty(parentName)){
			model.addAttribute("parentId", parentId);
			model.addAttribute("parentName", parentName);
		}
		return "system/department/department_add";
	}
	
	/**
	 * 添加部门
	 */
	@PostMapping("/system/department/add")
	@ResponseBody
	public AdminResultVo addDepartment(@RequestBody DepartmentDto department) throws Exception{
		DepartmentDto obj = systemDepartmentService.addDepartment(department);
		return AdminResultVo.ok().setData(obj).setMsg("部门添加成功！");
	}
	
	/**
	 * 展示所有部门
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/system/department/treeview")
	@ResponseBody
	public List<DepartmentDto> getDepartmentTreeview() throws Exception{
		List<DepartmentDto> departmentList = systemDepartmentService.getDepartmentList();
		return departmentList;
	}
	
	/**
	 * 删除部门
	 * @param id
	 * @return
	 */
	@PostMapping("/system/department/delete")
	@ResponseBody
	public AdminResultVo deleteDepartment(Integer id){
		Integer[] ids = new Integer[]{id};
		systemDepartmentService.batchDeleteDepartment(ids);
		return AdminResultVo.ok().setMsg("部门删除成功！");
	}
	
	/**
	 * 批量删除部门
	 * @param ids
	 * @return
	 */
	@PostMapping("/system/department/batch_delete")
	@ResponseBody
	public AdminResultVo batchDeleteDepartment(@RequestParam("ids[]") Integer[] ids){
		systemDepartmentService.batchDeleteDepartment(ids);
		return AdminResultVo.ok().setMsg("部门批量删除成功！");
	}
	
	/**
	 * 显示修改对话框
	 * @throws Exception 
	 */
	@GetMapping("/system/department/update")
	public String toUpdateDepartment(Integer id, Model model) throws Exception{
		DepartmentPo department = systemDepartmentService.getDepartment(id);
		model.addAttribute("department", department);
		return "system/department/department_update";
	}
	
	/**
	 * 修改部门
	 * @throws Exception 
	 */
	@PostMapping("/system/department/update")
	@ResponseBody
	public AdminResultVo updateDepartment(@RequestBody DepartmentDto department) throws Exception{
		DepartmentDto obj = systemDepartmentService.updateDepartment(department);
		return AdminResultVo.ok().setData(obj).setMsg("部门修改成功！");
	}
	
	/**
	 * 部门树
	 * @return
	 */
	@PostMapping("/system/department/department_tree")
	@ResponseBody
	public AdminResultVo getPermissionTree(){
		List<TreeDto> list = systemDepartmentService.getDepartmentTree();
		return AdminResultVo.ok().setData(list).setMsg("获取部门树成功！");
	}
}
