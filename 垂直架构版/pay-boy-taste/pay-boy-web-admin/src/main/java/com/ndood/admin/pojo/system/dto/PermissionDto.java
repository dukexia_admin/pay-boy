package com.ndood.admin.pojo.system.dto;

import java.util.Date;

import com.ndood.admin.pojo.system.PermissionPo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 权限DTO类
 */
@Getter @Setter
@EqualsAndHashCode(callSuper=false)
public class PermissionDto extends PermissionPo {
	private static final long serialVersionUID = 6650285921381454775L;
	private Integer parentId;
	
	public PermissionDto(String name,PermissionPo parent) {
		super();
		super.setName(name);
		super.setParent(parent);
	}

	public PermissionDto() {
		super();
	}

	public PermissionDto(Integer parentId,String name) {
		super();
		super.setName(name);
		this.parentId = parentId;
	}
	
	public PermissionDto(Integer id, String name, String desc, String icon, Integer sort, Integer type, String url,
			Integer status, Date createTime, Date updateTime, Integer parentId) {
		super();
		super.setId(id);
		super.setName(name);
		super.setDesc(desc);
		super.setIcon(icon);
		super.setSort(sort);
		super.setType(type);
		super.setUrl(url);
		super.setStatus(status);
		super.setCreateTime(createTime);
		super.setUpdateTime(updateTime);
		this.parentId = parentId;
	}
}
