package com.ndood.admin.pojo.system.query;

import java.util.Date;

import com.ndood.admin.pojo.system.RegionPo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 字典查询类
 * 
 * @author ndood
 */
@Getter @Setter
@EqualsAndHashCode(callSuper=false)
public class RegionQuery extends RegionPo {
	private static final long serialVersionUID = -7156384507309423184L;
	private String keywords;
	private Date startTime;
	private Date endTime;
}
