package com.ndood.core.validate.code.sms;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.web.context.request.ServletWebRequest;

import com.ndood.core.properties.SecurityProperties;
import com.ndood.core.validate.code.ValidateCode;
import com.ndood.core.validate.code.ValidateCodeGenerator;

/**
 * 添加短信验证码生成器类
 * 结合ValidateCodeBeanConfig @ConditionalOnMissingBean
 * 这样可以方便在imooc-security-demo中新配@Component("smsCodeGenerator") 覆盖imooc-security-core中的
 * @author ndood
 */
public class SmsCodeGenerator implements ValidateCodeGenerator{
	
	private SecurityProperties securityProperties;

	public SecurityProperties getPaymentProperties() {
		return securityProperties;
	}

	public void setPaymentProperties(SecurityProperties securityProperties) {
		this.securityProperties = securityProperties;
	}

	/**
	 * 生成短信验证码
	 */
	@Override
	public ValidateCode generate(ServletWebRequest request) {
		String code = RandomStringUtils.randomNumeric(securityProperties.getCode().getSms().getLength());
		return new ValidateCode(code,securityProperties.getCode().getSms().getExpireIn());
	}
	
}
