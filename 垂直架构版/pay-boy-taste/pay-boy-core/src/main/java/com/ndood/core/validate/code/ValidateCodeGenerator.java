package com.ndood.core.validate.code;

import org.springframework.web.context.request.ServletWebRequest;

/**
 * 添加验证码生成器接口
 * @author ndood
 */
public interface ValidateCodeGenerator {

	/**
	 * 生成通用验证码
	 */
	ValidateCode generate(ServletWebRequest request);
}