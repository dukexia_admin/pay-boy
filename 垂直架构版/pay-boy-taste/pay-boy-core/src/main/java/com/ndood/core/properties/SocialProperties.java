package com.ndood.core.properties;
/**
 * Social属性配置类
 */
public class SocialProperties {
	
	private String filterProcessesUrl = "/auth";
	
	private QQProperties qq = new QQProperties();
	
	private WeixinProperties weixin = new WeixinProperties();

	private MiniProgramProperties mini = new MiniProgramProperties();
	
	public QQProperties getQq() {
		return qq;
	}

	public void setQq(QQProperties qq) {
		this.qq = qq;
	}

	public String getFilterProcessesUrl() {
		return filterProcessesUrl;
	}

	public void setFilterProcessesUrl(String filterProcessesUrl) {
		this.filterProcessesUrl = filterProcessesUrl;
	}

	public WeixinProperties getWeixin() {
		return weixin;
	}

	public void setWeixin(WeixinProperties weixin) {
		this.weixin = weixin;
	}

	public MiniProgramProperties getMini() {
		return mini;
	}

	public void setMini(MiniProgramProperties mini) {
		this.mini = mini;
	}
	
}