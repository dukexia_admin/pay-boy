package com.ndood.merchant.pojo.comm.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * 用户查询条件类
 * @author ndood
 */
public class UserQueryCondition {
	/**
	 * 属性自定义swagger描述
	 */
	@ApiModelProperty(value="用户年龄起始值")
	private int age;
	
	/**
	 * 属性自定义swagger描述
	 */
	@ApiModelProperty(value="用户年龄终止值")
	private int ageTo;
	
	private String xxx;

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getAgeTo() {
		return ageTo;
	}

	public void setAgeTo(int ageTo) {
		this.ageTo = ageTo;
	}

	public String getXxx() {
		return xxx;
	}

	public void setXxx(String xxx) {
		this.xxx = xxx;
	}
	
}