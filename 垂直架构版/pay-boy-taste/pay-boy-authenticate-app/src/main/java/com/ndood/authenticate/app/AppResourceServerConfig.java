package com.ndood.authenticate.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.social.security.SpringSocialConfigurer;

import com.ndood.authenticate.app.openid.OpenIdAuthenticationSecurityConfig;
import com.ndood.core.authentication.mobile.SmsCodeAuthenticationSecurityConfig;
import com.ndood.core.authorize.AuthorizeConfigManager;
import com.ndood.core.validate.code.ValidateCodeSecurityConfig;

/**
 * oauth2资源服务器配置
 * 加了这个等于把app定义成资源服务器，访问资源的时候要先通过认证
 */
@Configuration
@EnableResourceServer
public class AppResourceServerConfig extends ResourceServerConfigurerAdapter{
	
	@Autowired
	private AuthenticationSuccessHandler appAuthenticationSuccessHandler;
	
	@Autowired
	private AuthenticationFailureHandler appAuthenticationFailureHandler;
	
	@Autowired
	private SmsCodeAuthenticationSecurityConfig smsCodeAuthenticationSecurityConfig;
	
	@Autowired
	private SpringSocialConfigurer springSocialSecurityConfig;
	
	@Autowired
	private ValidateCodeSecurityConfig validateCodeSecurityConfig;
	
	@Autowired
	private OpenIdAuthenticationSecurityConfig openIdAuthenticationSecurityConfig;
	
	@Autowired
	private AuthorizeConfigManager authorizeConfigManager;
	
	/**
	 * 未认证访问消息格式
	 */
	@Autowired(required=false)
	private AuthenticationEntryPoint unAuthenticatedAccessHandler;

	/**
	 * 未授权访问消息格式
	 */
	@Autowired(required=false)
	private AccessDeniedHandler unAuthorizedAccessHandler;

	/**
	 * 配置资源返回格式
	 */
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		super.configure(resources);
		if(unAuthenticatedAccessHandler!=null) {
			resources.authenticationEntryPoint(unAuthenticatedAccessHandler);
		}
		if(unAuthorizedAccessHandler!=null) {
			resources.accessDeniedHandler(unAuthorizedAccessHandler);
		}
	}
	
	/**
	 * 配置http
	 */
	@Override
	public void configure(HttpSecurity http) throws Exception {

		http
			.apply(validateCodeSecurityConfig)
				.and()
			.apply(smsCodeAuthenticationSecurityConfig)
				.and()
			.apply(springSocialSecurityConfig)
				.and()
			
			.formLogin()
				.loginPage("/authentication/require")
				.loginProcessingUrl("/authentication/form")
				.successHandler(appAuthenticationSuccessHandler)
				.failureHandler(appAuthenticationFailureHandler)
				.and()
			
			.apply(openIdAuthenticationSecurityConfig)
					.and()			
				
			.csrf().disable();
		
		// 调用公共的权限管理配置
		authorizeConfigManager.config(http.authorizeRequests());
	}	
}
