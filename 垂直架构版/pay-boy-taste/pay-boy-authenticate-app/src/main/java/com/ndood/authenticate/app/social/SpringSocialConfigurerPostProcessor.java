package com.ndood.authenticate.app.social;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import com.ndood.core.social.SpringSocialConfig;

/**
 * 使得所有bean在初始化之后都进行该操作
 * SocialConfig中的ndoodSocialSecurityConfig bean初始化好后，将signUpUrl（需要注册）改成app的，而非浏览器的重定向页面
 */
@Component
public class SpringSocialConfigurerPostProcessor implements BeanPostProcessor {

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}

	/* 
	 * 将signUpUrl改成app的接口地址
	 * 当bean初始化以后，将signupUrl改掉
	 * FIXME 这样的处理方式有问题，当用户根据http://18p88z8050.imwork.net/qqLogin/weixin?code=YxquV8&state=a2104078-bfaf-4c97-a75b-acb57cf4c56d访问的时候发生302跳转到/social/user
	 * 而app请求应该不会跳转，所以这边需要访问一次/social/user。ps：这套代码的硬伤所在
	 */
	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if(StringUtils.equals(beanName, "springSocialSecurityConfig")){
			SpringSocialConfig config = (SpringSocialConfig)bean;
			config.signupUrl("/social/user");
			return config;
		}
		return bean;
	}

}