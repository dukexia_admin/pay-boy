package com.ndood.authenticate.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import com.ndood.authenticate.app.jwt.AppJwtTokenEnhancer;
import com.ndood.core.properties.SecurityProperties;

/**
 * 将Inmemory方式改成token持久化方式
 */
@Configuration
public class AppTokenStoreConfig {
	
	@Autowired
	private RedisConnectionFactory redisConnectionFactory;
	
	/**
	 * 产生token自动存到redis中
	 * 如果配置文件中的imooc.security.oauth2.storeType=redis时，这个配置生效
	 */
	@Bean
	@ConditionalOnProperty(prefix = "ndood.security.oauth2", name="storeType", havingValue = "redis")
	public TokenStore redisTokenStore(){
		return new AppRedisTokenStore(redisConnectionFactory);
	}
	
	/**
	 * 告诉它用jwt方式获取token
	 * 如果配置文件中的imooc.security.oauth2.storeType=jwt时，这个类的所有配置都会生效
	 * matchIfMissing = true 如果配置文件中不写配置项，这个类也会生效
	 */
	@Configuration
	@ConditionalOnProperty(prefix = "ndood.security.oauth2", name="storeType", havingValue = "jwt", matchIfMissing = true)
	public static class JwtTokenConfig{
		
		@Autowired
		private SecurityProperties securityProperties;
		
		/**
		 * 处理token存储
		 */
		@Bean
		public TokenStore jwtTokenStore(){
			return new JwtTokenStore(jwtAccessTokenConverter());
		}

		/**
		 * 处理token生成 指定一个秘钥，对它进行签名
		 */
		@Bean
		public JwtAccessTokenConverter jwtAccessTokenConverter(){
			JwtAccessTokenConverter accessTokenConverter = new JwtAccessTokenConverter();
			//  配置签名用的秘钥
			accessTokenConverter.setSigningKey(securityProperties.getOauth2().getJwtSignKey()); 
			return accessTokenConverter;
		}
		
		/**
		 * 在token中加入自定义附加
		 * 定义成默认bean，可以被业务层覆盖
		 */
		@Bean
		@ConditionalOnMissingBean(name = "jwtTokenEnhancer")
		public TokenEnhancer jwtTokenEnhancer(){
			return new AppJwtTokenEnhancer();
		}
	}
}
